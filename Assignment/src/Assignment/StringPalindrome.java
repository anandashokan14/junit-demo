package Assignment;

import java.util.Scanner;

public class StringPalindrome {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the String to Check:");
		String str=sc.nextLine();
		char[] StrArray =str.toCharArray();
		String StrNew = "";
		for (int i = StrArray.length-1;i>=0;i--) {
			StrNew = StrNew.concat(String.valueOf(StrArray[i]));
		}
		System.out.println("====================================");
		if (str.equalsIgnoreCase(StrNew)) {
			System.out.println(str+" is Palindrome");
		}else {
			System.out.println(str+" is not Palindrome");
		}
	
	}

}
