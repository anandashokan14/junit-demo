package Assignment;

public class SwapProgram {

	public static void main(String[] args) {
		int a = 10, b = 20;
		System.out.println("Before Swaping\nA="+a+" B="+b);
		a = a+b;
		b = a-b;
		a = a-b;
		System.out.println("After Swaping\nA="+a+" B="+b);
	}

}
