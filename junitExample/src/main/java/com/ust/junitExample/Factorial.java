package com.ust.junitExample;

public class Factorial {

	public int findFactorial(int input) {
		
		if(input<0) {
			return -1;
		}
		int fact = 1;
		for (int i = 1; i <= input; i++) {
			fact = fact * i;

		}
		return fact;
	}
}
